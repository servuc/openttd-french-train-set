FROM python:3.9

RUN pip3 install Pillow && pip3 install ply && pip3 install nml

VOLUME /work/

WORKDIR /work/

CMD ./make_trains_pnml.sh && ./make_main_pnml.sh && nmlc ofts.pnml