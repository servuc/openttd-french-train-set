# Palette problem

[[_TOC_]]

If `nmlc` returns :

 - *8bpp image does not have a palette*
 - *Invalid palette; does not contain 256 entries.*

## Download and install palette

https://dev.openttdcoop.org/documents/1

For that project *DOS palette is used*. Download it.

 - Start Gimp
 - Windows > Dockable Dialogs > Palettes
 - Right click on a palette > Show in files manager
 - Paste download file here
 - Restart Gimp

## Apply palette to PNG

 - Open PNG with Gimp
 - Image > Mode > Indexed colors ...
 - User palette > Choose TTD palette
 - Uncheck: Remove useless colors ...
 - Convert
 - Export file as PNG