# OpenTTD - Ensemble de trains français

[[_TOC_]]

## Objectifs - Goals

Je sais que plusieurs projets d'ensemble de trains français existent sur la toile, mais je n'ai pas trouvé un ensemble complet, ou que l'on peut contribuer facilement. Voilà pourquoi ce projet est né !

Les rails sont inspirés de https://dev.openttdcoop.org/projects/dutchtracks.

I know that there is a lot of projects about french trains set. Or project when we can contribute easily. It's the reason why that project is born !

Rails are inspired from https://dev.openttdcoop.org/projects/dutchtracks.

## Docker

    docker build -t pnml_maker .
    docker run -ti --rm -v `pwd`:/work pnml_maker:latest

## TODO

### Comment compléter ? - 

Les images seront en au format PNG (et JSON) dans le dossier `/gfx/MODEL/` et les scripts en NML dans le dossier `/scripts/MODEL/`. Dans le cas d'un autorail qui aurait des wagons correspondant à la locomotive, le dossier sera `MODEL-W`.<br />
Les fichiers JSON peuvent être différent final. Ils sont une esquisse !<br />
Les fichiers seront nommés en *kébab-case `LIVREE.format`.

Il y a aura une ligne pour chaque livrée. L'objectif est de laisser au joueur le choix du coloris (réaménagement).<br />
Pour le dessin, sur la hauteur `5m = 10px`, la longueur `4m = 5px` (max: `32px`) et la largeur (`depth` dans l'outil) `12px`.

Veuillez mettre votre ligne en fonction de l'année de création.

N'oubliez pas de lire : https://newgrf-specs.tt-wiki.net/wiki/PalettesAndCoordinates

### How to complete ?

Pictures will be in PNG (and JSON) `/gfx/MODEL/` dir and script in NML in `/scripts/MODEL/` dir. In case of wagon design like locomotive, dir will be called `MODEL-W`.<br />
JSON files can be not the final design !<br />
Files should be call in kebab-case* : `PAINT.format`.

It should be one line for each different paint. Goal is to allow player to choose paint (re-arrange).<br />
For the graphics, height is `5m = 10px`, length is `4m = 5px` (max: `32px`) and width (`depth` in tool) `12px`.

Please order line by creating year.

Do not missing to read : https://newgrf-specs.tt-wiki.net/wiki/PalettesAndCoordinates

### Messages de commit - Commit messages

Veillez à faire un seul modèle à la fois. Mais il est possible de faire plusieurs livrées à la fois. Si vous complétez toutes les livrées, ne le spécifiez pas dans le message.<br />
Veuillez envoyer votre travail sur une nouvelle branche Git.

Please, make a commit for each model. But you can made one commit for multiple paint. If you complete all paints, do not mention it.<br />
Please, send you work on a new Git branch.

Ajouter un nouveau train :<br />
Add a train :

    Add: MODEL (LIVREE) GFX & NML
    Add: MODEL (LIVREE) GFX
    Add: MODEL (LIVREE) NML

Mettre à un nouveau train :<br />
Update a train :

    Update: MODEL (LIVREE) GFX & NML
    Update: MODEL (LIVREE) GFX
    Update: MODEL (LIVREE) NML

Ajouter un élément à la liste : <br />
Add list item :

    List-Add: MODEL (LIVREE)

Mettre à jour la liste : <br />
Update list :

    List-Update: MODEL (LIVREE)

### Comprendre le tableau - Understand table

| Nom | Description |
| --- | --- |
| Nom | Nom (Name) |
| Type | <ul><li>Wagon</li><li>Vapeur (Steam)</li><li>Diesel (Diesel)</li><li>Électricité (Electricity)</li></ul> |
| PNG | ✅ ou (or) ❌ avec le nom du fichier (with file name) |
| NML | ✅ ou (or) ❌ avec le nom du fichier (with file name)  |
| Photo | Lien (Link) |
| Auteur | Créateur de l'image (User creator) |
| Vitesse max | En km/h (Speed max in km/h) |
| Livrée | Peinture (Paint) |
| Date création | (Creating date) |
| Date fin | (Ending date) |
| Prix | En €, permettra de déduire le `cost_factor` (Price in € help to know `cost_factor`) |
| Prix d'utilisation | En €, permettra de déduire le `running_cost_factor` (Price in € help to know `running_cost_factor`) |
| Année d'utilisation | Temps maximum d'utilisation `vehicle_life` (Max years usage `vehicle_life`) |

### Liste - List

| Nom | Type | PNG | NML | Photo | Auteur | Vit. max | Livrée | Date créa. | Date fin | Prix | Prix d'uti. | Année d'uti. |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| CC1100 | Électrique | ❌ | ❌ | | | 30 | Couleur du joueur | 1937 | 1948 | | | 70 |
| CC1100 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_186.jpg) | | 30 | Orange | 1937 | 1948 | | | 70 |
| CC1100 | Électrique | ❌ | ❌ | [X](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/CC-1101_Villeneuve_avril_1986.jpg/260px-CC-1101_Villeneuve_avril_1986.jpg) | | 30 | Bleu | 1937 | 1948 | | | 70 |
| X3600 | Diesel | ❌ | ❌ | | | 120 | Couleur du joueur | 1948 | 1977 | | | 30 |
| X3600 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_7264.jpg) | | 120 | | 1948 | 1977 | | | 30 |
| CC7100 | Électrique | ❌ | ❌ | | | 160 | Couleur du joueur | 1952 | 1955 | | | 50 |
| CC7100 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_12860.jpg) | | 160 | Corail | 1952 | 1955 | | | 50 |
| X2800 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1277.jpg) | | 120 | Bleu | 1957 | 1962 | | | 60 |
| X2800 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_9225.jpg) | | 120 | Rouge | 1957 | 1962 | | | 60 |
| BB9200 | Électrique | ✅ | ✅ | | | 200 | Couleur du joueur | 1957 | 1964 | | | 45 |
| BB9200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_64.jpg) | | 200 | Fret | 1957 | 1964 | | | 45 |
| BB9200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_70.jpg) | | 200 | Béton | 1957 | 1964 | | | 45 |
| BB9200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_71.jpg) | | 200 | Bleu | 1957 | 1964 | | | 45 |
| BB9200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_3773.jpg) | | 200 | En voyage | 1957 | 1964 | | | 45 |
| BB9200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_3773.jpg) | | 200 | MultiService | 1957 | 1964 | | | 45 |
| BB9200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_130.jpg) | | 200 | Capitole | 1957 | 1964 | | | 45 |
| BB9200 | Électrique | ✅ | ✅ | [X](https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/SNCF_BB_9282-JPVL.jpg/269px-SNCF_BB_9282-JPVL.jpg) | | 200 | Corail | 1957 | 1964 | | | 45 |
| CC65000 | Diesel | ❌ | ❌ | | | 140 | Couleur du joueur | 1957 | 1958 | | | 30 |
| CC65000 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_10260.jpg) | | 140 | Agrivap | 1957 | 1958 | | | 30 |
| CC65000 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1520.jpg) | | 140 | Bleu | 1957 | 1958 | | | 30 |
| BB9400 | Électrique | ✅ | ✅ | | | 160 | Couleur du joueur (Arzens) | 1959 | 1964 | | | 40 |
| BB9400 | Électrique | ✅ | ✅ | [X](https://upload.wikimedia.org/wikipedia/commons/a/a4/SNCF_BB_9435_Arzens.jpg) | | 160 | Arzens | 1959 | 1964 | | | 40 |
| BB9400 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_1591.jpg) | | 160 | Bleu | 1959 | 1964 | | | 40 |
| BB9600 | Électrique | ✅ | ✅ | | | 140 | Couleur du joueur (TGV Atlantique) | 1990 | 1994 | | | 45 |
| BB9600 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_418.jpg) | | 140 | Béton | 1990 | 1994 | | | 45 |
| BB9600 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_435.jpg) | | 140 | TGV Atlantique | 1990 | 1994 | | | 45 |
| BB9700 | Électrique | ✅ | ✅ | | | 160 | Couleur du joueur | 1960 | 1962 | | | 50 |
| BB9700 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_443.jpg) | | 160 | Corail | 1960 | 1962 | | | 50 |
| BB66000 | Diesel | ❌ | ❌ | | | 120 | Couleur du joueur | 1961 | 1964 | | | 60 |
| BB66000 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1488.jpg) | | 120 | Bleu | 1960 | 1968 | | | 60 |
| BB66000 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_10896.jpg) | | 120 | Livrée originelle | 1960 | 1968 | | | 60 |
| BB66000 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_8642.jpg) | | 120 | Fret | 1960 | 1968 | | | 60 |
| BB66600 | Diesel | ❌ | ❌ | | | 120 | Couleur du joueur | 1960 | 1962 | | | 55 |
| BB66600 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_3304.jpg) | | 120 | Isabelle | 1960 | 1962 | | | 55 |
| BB66600 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_7260.jpg) | | 120 | Karine | 1960 | 1962 | | | 55 |
| BB66600 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_5344.jpg) | | 120 | Le Messie | 1960 | 1962 | | | 55 |
| BB66600 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_12914.jpg) | | 120 | AT3 603 VFLI | 1960 | 1962 | | | 55 |
| BB66700 | Diesel | ❌ | ❌ | | | 80 | Couleur du joueur | 1961 | 1964 | | | 55 |
| BB66700 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_12438.jpg) | | 80 | Fret | 1961 | 1964 | | | 55 |
| BB66700 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1051.jpg) | | 80 | Arzens | 1961 | 1964 | | | 55 |
| X4300 | Diesel | ✅ | ✅ | | | 120 | Couleur du joueur | 1963 | 1970 | | | 60 |
| X4300 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_4176.jpg) | | 120 | Rouge | 1963 | 1970 | | | 60 |
| X4300-W | Wagon | 🚧 | ❌ | [X](http://letraindalain.free.fr/img/img_4176.jpg) | | 120 | Rouge | 1963 | 1970 | | | 60 |
| X4300 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_4228.jpg) | | 120 | Vert | 1963 | 1970 | | | 60 |
| X4300-W | Wagon | 🚧 | ❌ | [X](http://letraindalain.free.fr/img/img_4228.jpg) | | 120 | Vert | 1963 | 1970 | | | 60 |
| X4500 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_1309.jpg) | | 120 | Rouge | 1963 | 1970 | | | 60 |
| X4500-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1309.jpg) | | 120 | Rouge | 1963 | 1970 | | | 60 |
| X4500 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_1311.jpg) | | 120 | Vert Picardie | 1963 | 1970 | | | 60 |
| X4500-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1311.jpg) | | 120 | Vert Picardie | 1963 | 1970 | | | 60 |
| X4500 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_7406.jpg) | | 120 | Bleu | 1963 | 1970 | | | 60 |
| X4500-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_7406.jpg) | | 120 | Bleu | 1963 | 1970 | | | 60 |
| X4500 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_12658.jpg) | | 120 | Blanc TPCF | 1963 | 2070 | | | 60 |
| X4500-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_12658.jpg) | | 120 | Blanc TPCF | 1963 | 2070 | | | 60 |
| BB67000 | Diesel | ✅ | ❌ | | | 140 | Couleur du joueur | 1963 | 1968 | | | 45 |
| BB67000 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_1074.jpg) | | 140 | Bleu | 1963 | 1968 | | | 45 |
| BB67200 | Diesel | ✅ | ❌ | | | 90 | Couleur du joueur | 1963 | 1968 | | | 50 |
| BB67200 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_5768.jpg) | | 90 | Bleu | 1980 | 2004 | | | 50 |
| BB67200 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_9678.jpg) | | 90 | Infra | 1980 | 2004 | | | 50 |
| BB67300 | Diesel | ❌ | ❌ | | | 140 | Couleur du joueur | 1967 | 1969 | | | 50 |
| BB67300 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_4097.jpg) | | 140 | Bleu | 1967 | 1969 | | | 50 |
| BB67300 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_6398.jpg) | | 140 | Corail+ | 1967 | 1969 | | | 50 |
| BB67300 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_10910.jpg) | | 140 | Bleu Isabelle | 1967 | 1969 | | | 50 |
| BB67300 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1110.jpg) | | 140 | Fret | 1967 | 1969 | | | 50 |
| BB67400 | Diesel | ❌ | ❌ | | | 140 | Couleur du joueur | 1969 | 1975 | | | 60 |
| BB67400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_4416.jpg) | | 140 | Corail+ | 1969 | 1975 | | | 60 |
| BB67400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_2997.jpg) | | 140 | Bleu | 1969 | 1975 | | | 60 |
| BB67400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_6981.jpg) | | 140 | En voyage | 1969 | 1975 | | | 60 |
| BB67400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_8903.jpg) | | 140 | Fret | 1969 | 1975 | | | 60 |
| BB67400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_9679.jpg) | | 140 | Infra | 1969 | 1975 | | | 60 |
| BB67400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_14186.jpg) | | 140 | Capitole | 1969 | 1975 | | | 60 |
| BB67400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_14186.jpg) | | 140 | Capitole | 1969 | 1975 | | | 60 |
| A1A A1A 68000 | Diesel | ❌ | ❌ | | | 130 | Couleur du joueur | 1963 | 1968 | | | 65 |
| A1A A1A 68000 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1466.jpg) | | 130 | Bleu | 1963 | 1968 | | | 65 |
| A1A A1A 68000 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_931.jpg) | | 130 | Fret | 1963 | 1968 | | | 65 |
| A1A A1A 68000 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1466.jpg) | | 130 | Bleu | 1963 | 1968 | | | 65 |
| A1A A1A 68500 | Diesel | ❌ | ❌ | | | 130 | Couleur du joueur | 1963 | 1968 | | | 60 |
| A1A A1A 68500 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1470.jpg) | | 130 | Bleu | 1963 | 1968 | | | 60 |
| A1A A1A 68500 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_6989.jpg) | | 130 | Infra | 1963 | 1968 | | | 60 |
| A1A A1A 68500 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_6990.jpg) | | 130 | Fret | 1963 | 1968 | | | 60 |
| BB69000 | Diesel | ❌ | ❌ | | | 140 | Couleur du joueur | 1961 | 1964 | | | 20 |
| BB69000 | Diesel | ❌ | ❌ | [X](https://fr.wikipedia.org/wiki/BB_69000) | | 140 | Bleu | 1961 | 1964 | | | 20 |
| BB70000 | Diesel | ❌ | ❌ | | | 140 | Couleur du joueur | 1966 | 1966 | | | 20 |
| BB70000 | Diesel | ❌ | ❌ | [X](https://fr.wikipedia.org/wiki/CC_70000) | | 140 | Bleu | 1966 | 1966 | | | 20 |
| BB8500 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_3725.jpg) | | 140 | Béton | 1964 | 1974 | | | 40 |
| BB8500 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_359.jpg) | | 140 | Fret | 1964 | 1974 | | | 40 |
| BB8500 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_362.jpg) | | 140 | Bleu | 1964 | 1974 | | | 40 |
| BB8500 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_364.jpg) | | 140 | En voyage | 1964 | 1974 | | | 40 |
| BB8500 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_366.jpg) | | 140 | Ile de France | 1964 | 1974 | | | 40 |
| BB8500 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_3637.jpg) | | 140 | Fret | 1964 | 1974 | | | 40 |
| BB8500 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_371.jpg) | | 140 | Multiservice | 1964 | 1974 | | | 40 |
| BB8700 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_396.jpg) | | 90 | Béton | 1965 | 1972 | | | 40 |
| BB8700 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_396.jpg) | | 90 | Béton | 1965 | 1972 | | | 40 |
| BB69200 | Diesel | ❌ | ❌ | | | 120 | Couleur du joueur | 1965 | 1968 | | | 55 |
| BB69200 | Diesel | ❌ | ❌ | [X](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/69200_LGVRR.jpg/1280px-69200_LGVRR.jpg) | | 120 | Infra | 1965 | 1968 | | | 55 |
| BB9300 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_11445.jpg) | | 160 | Béton | 1967 | 1969 | | | 50 |
| BB9300 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_34.jpg) | | 160 | MultiService | 1967 | 1969 | | | 50 |
| BB9300 | Électrique | ❌ | ❌ | [X](https://upload.wikimedia.org/wikipedia/commons/8/82/SNCF_BB_9302.jpg) | | 160 | Bleu | 1967 | 1969 | | | 50 |
| BB9300 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_37.jpg) | | 160 | En voyage | 1967 | 1969 | | | 50 |
| BB66400 | Diesel | ❌ | ❌ | | | 120 | Couleur du joueur | 1968 | 1971 | | | 55 |
| BB66400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1040.jpg) | | 120 | Fret | 1968 | 1971 | | | 55 |
| BB66400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_13913.jpg) | | 120 | Infra verte | 1968 | 1971 | | | 55 |
| BB66400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_5342.jpg) | | 120 | Infra Bleu | 1968 | 1971 | | | 55 |
| BB69400 | Diesel | ❌ | ❌ | | | 120 | Couleur du joueur | 1965 | 1968 | | | 55 |
| BB69400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_14669.jpg) | | 120 | Infra Verte | 1968 | 1971 | | | 55 |
| BB69400 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_12020.jpg) | | 120 | Infra Jaune | 1968 | 1971 | | | 55 |
| CC6500 | Électrique | ✅ | ✅ | | | 140 | Couleur du joueur | 1969 | 1975 | | | 60 |
| CC6500 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_256.jpg) | | 140 | TEE Arzens | 1969 | 1975 | | | 40 |
| CC6500 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_11537.jpg) | | 140 | Fret | 1969 | 1975 | | | 40 |
| CC6500 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_245.jpg) | | 140 | Maurienne | 1969 | 1975 | | | 40 |
| CC6500 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_253.jpg) | | 140 | Beton | 1969 | 1975 | | | 40 |
| CC21000 | Électrique | ✅ | ✅ | | | 220 | Couleur du joueur | 1969 | 1974 | | | 55 |
| CC21000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_602.jpg) | | 220 | Grand confort | 1969 | 1974 | | | 55 |
| BB15000 | Électrique | ✅ | ✅ | | | 160 | Couleur du joueur | 1971 | 1978 | | | 55 |
| BB15000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_8720.jpg) | | 160 | Arzens | 1976 | 1986 | | | 55 |
| BB15000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_4812.jpg) | | 160 | Corail | 1976 | 1986 | | | 55 |
| BB15000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_4814.jpg) | | 160 | Corail Prototype | 1976 | 1986 | | | 55 |
| BB15000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_5125.jpg) | | 160 | En Voyage | 1976 | 1986 | | | 55 |
| BB7200 | Électrique | ✅ | ✅ | | | 200 | Couleur du joueur | 1976 | 1985 | | | 60 |
| BB7200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_300.jpg) | | 200 | Corail | 1976 | 1985 | | | 60 |
| BB7200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_6232.jpg) | | 200 | En voyage | 1976 | 1985 | | | 60 |
| BB7200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_2949.jpg) | | 200 | Béton | 1976 | 1985 | | | 60 |
| BB7200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_4471.jpg) | | 200 | Grise | 1976 | 1985 | | | 60 |
| BB7200 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_2951.jpg) | | 200 | Fret | 1976 | 1985 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | | | 200 | Couleur du joueur | 1976 | 1986 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_6923.jpg) | | 200 | Beton | 1976 | 1986 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_2718.jpg) | | 200 | Fret | 1976 | 1986 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_5138.jpg) | | 200 | Gris | 1976 | 1986 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_9590.jpg) | | 200 | Aubrais | 1976 | 1986 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_617.jpg) | | 200 | En Voyage | 1976 | 1986 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_6015.jpg) | | 200 | Transilien | 1976 | 1986 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_8938.jpg) | | 200 | Transilien PAC | 1976 | 1986 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_10495.jpg) | | 200 | TER PACA | 1976 | 1986 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_12254.jpg) | | 200 | Corail | 1976 | 1986 | | | 60 |
| BB22000 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_12769.jpg) | | 200 | Transilien PAC - Pole de conduite | 1976 | 1986 | | | 60 |
| TGV | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_14913.jpg) | | 300 | Sud-Est | 1978 | 1986 | | | 40 |
| TGV | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_11565.jpg) | | 300 | Carmillon | 1978 | 1986 | | | 40 |
| TGV | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_7015.jpg) | | 300 | Bleu - Gris | 1978 | 1986 | | | 40 |
| X4300 | Diesel | ✅ | ✅ | | | 120 | Couleur joueur-2nd | 1980 | 1985 | | | 60 |
| X4300 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_2439.jpg) | | 120 | Bleu-Isabelle-2nd | 1980 | 1985 | | | 60 |
| X4300 | Diesel | ✅ | ✅ | | | 120 | Vert-Perroquet-2nd | 1980 | 1985 | | | 60 |
| X4300 | Diesel | ✅ | ✅ | | | 120 | Rouge-Vermillon-2nd | 1980 | 1985 | | | 60 |
| X4300 | Diesel | ✅ | ✅ | | | 120 | Jaune-Lithos-2nd | 1980 | 1985 | | | 60 |
| X4300-W | Wagon | ❌ | ❌ | | | 120 | Couleur joueur-2nd | 1980 | 1985 | | | 60 |
| X4300-W | Wagon | 🚧 | ❌ | [X](http://letraindalain.free.fr/img/img_2439.jpg) | | 120 | Bleu-Isabelle-2nd | 1980 | 1985 | | | 60 |
| X4300-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_2439.jpg) | | 120 | Vert-Perroquet-2nd | 1980 | 1985 | | | 60 |
| X4300-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_2439.jpg) | | 120 | Rouge-Vermillon-2nd | 1980 | 1985 | | | 60 |
| X4300-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_2439.jpg) | | 120 | Jaune-Lithos-2nd | 1980 | 1985 | | | 60 |
| X4500 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_1312.jpg) | | 120 | Jaune-2nd | 1980 | 1985 | | | 60 |
| X4500-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_1312.jpg) | | 120 | Jaune-2nd | 1980 | 1985 | | | 60 |
| X4500 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_4171.jpg) | | 120 | Vert-2nd | 1980 | 1985 | | | 60 |
| X4500-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_4171.jpg) | | 120 | Vert-2nd | 1980 | 1985 | | | 60 |
| X4500 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_8193.jpg) | | 120 | ATTCV-2nd (Orange) | 1980 | 1985 | | | 60 |
| X4500-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_8193.jpg) | | 120 | ATTCV-2nd (Orange) | 1980 | 1985 | | | 60 |
| X4500 | Diesel | ✅ | ❌ | [X](http://letraindalain.free.fr/img/img_4172.jpg) | | 120 | Bleu-2nd | 1980 | 1985 | | | 60 |
| X4500-W | Wagon | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_4172.jpg) | | 120 | Bleu-2nd | 1980 | 1985 | | | 60 |
| TGV | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_2302.jpg) | | 270 | La Poste | 1984 | 2015 | | | 40 |
| BB26000 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_6767.jpg) | | 200 | Béton | 1988 | 1998 | | | 60 |
| BB26000 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_3446.jpg) | | 200 | En voyage | 1988 | 1998 | | | 60 |
| BB26000 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_12259.jpg) | | 200 | Carmillon | 1988 | 1998 | | | 60 |
| BB26000 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_12259.jpg) | | 200 | Grise | 1988 | 1998 | | | 60 |
| BB26000 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_13888.jpg) | | 200 | TER Grand est | 1988 | 1998 | | | 60 |
| BB26000 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_4992.jpg) | | 200 | Fret | 1988 | 1998 | | | 60 |
| BB26000 | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_8965.jpg) | | 200 | Corail | 1988 | 1998 | | | 60 |
| BB26000 | Électrique | ❌ | ❌ | [X](https://upload.wikimedia.org/wikipedia/commons/1/16/BB26070.JPG) | | 200 | TER Alsace | 1988 | 1998 | | | 60 |
| TGV | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_8085.jpg) | | 320 | Duplex | 1995 | | | | 40 |
| X73500 | Diesel | ✅ | ✅ | | | 140 | Couleur du joueur | 1999 | | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_6482.jpg) | | 140 | TER | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_6482.jpg) | | 140 | TER | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_5241.jpg) | | 140 | Limousin | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_13603.jpg) | | 140 | Pays de Loire | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_11298.jpg) | | 140 | Aquitaine | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_3071.jpg) | | 140 | Languedoc Roussillon | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_4190.jpg) | | 140 | Rhône Alpes (kaléidoscope) | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ❌ | ❌ | [X]() | | 140 | Rhône Alpes | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_7994.jpg) | | 140 | Picardie | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_3140.jpg) | | 140 | Bourgogne | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ❌ | ❌ | [X]() | | 140 | Auvergne | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_14171.jpg) | | 140 | MétroLor | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_10409.jpg) | | 140 | Haute Normandie | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_11299.jpg) | | 140 | CODAH | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_1344.jpg) | | 140 | Basse Normandie | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ❌ | ❌ | [X]() | | 140 | Champagne Ardenne | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_1346.jpg) | | 140 | Amitié Franco-Allemande | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ❌ | ❌ | [X]() | | 140 | Pas de Calais | 1999 | 2004 | | | 30 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_10135.jpg) | | 140 | Poitou Charente | 1999 | 2004 | | | 30 |
| TGV | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_5486.jpg) | | 300 | 30ème anniv. | 2008 | 2009 | | | 40 |
| TGV | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_8422.jpg) | | 300 | Chocolat | 2008 | 2009 | | | 40 |
| X73500 | Diesel | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_10133.jpg) | | 140 | Poitou Charente photovoltaïque | 2010 | 2012 | | | 30 |
| BB7600 | Électrique | ✅ | ✅ | | | 140 | Couleur du joueur | 2012 | 2015 | | | 60 |
| BB7600 | Électrique | ✅ | ✅ | [X](http://letraindalain.free.fr/img/img_7250.jpg) | | 140 | Transilien | 2012 | 2015 | | | 60 |
| X73500 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_11813.jpg) | | 140 | 2nd livrée Limousin | 2013 | | | | 30 |
| TGV | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_8770.jpg) | | 320 | OUIGO | 2013 | 2030 | | | 40 |
| TGV | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_9659.jpg) | | 300 | Allianz | 2013 | 2014 | | | 40 |
| X73500 | Diesel | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_7854.jpg) | | 140 | Picardie-2nd | 2016 | 2018 | | | 30 |
| X73500 | Diesel | ❌ | ❌ | [X]() | | 140 | Auvergne Rhône Alpes | 2016 | 2018 | | | 30 |
| X73500 | Diesel | ❌ | ❌ | [X]() | | 140 | Normandie | 2016 | 2018 | | | 30 |
| X73500 | Diesel | ❌ | ❌ | [X]() | | 140 | Nouvelle aquitaine | 2016 | 2018 | | | 30 |
| TGV | Électrique | ❌ | ❌ | [X](http://letraindalain.free.fr/img/img_14491.jpg) | | 320 | Carmillon INOUI | 2017 | 2030 | | | 40 |

### Pages utiles

#### A1A A1A 68000

 - https://fr.wikipedia.org/wiki/A1AA1A_68000
 - http://letraindalain.free.fr/galerieD.php?numserie=A1A%20A1A%2068000

#### BB7200

 - https://fr.wikipedia.org/wiki/BB_7200
 - http://letraindalain.free.fr/galerieP.php?numserie=BB%207200

#### BB7600

 - http://letraindalain.free.fr/galerieP.php?numserie=BB%207600

#### BB8500

 - https://fr.wikipedia.org/wiki/BB_8500
 - http://letraindalain.free.fr/galerieP.php?numserie=BB%208500

#### BB8700

 - https://fr.wikipedia.org/wiki/BB_8700
 - http://letraindalain.free.fr/galerieP.php?numserie=BB%208700

#### BB9200

 - https://fr.wikipedia.org/wiki/BB_9200
 - http://letraindalain.free.fr/galerieP.php?numserie=BB%209200

#### BB9300

 - https://fr.wikipedia.org/wiki/BB_9300
 - http://letraindalain.free.fr/galerieP.php?numserie=BB%209300

#### BB9400

 - https://fr.wikipedia.org/wiki/BB_9400
 - http://letraindalain.free.fr/galerieP.php?numserie=BB%209400

#### BB9600

 - https://fr.wikipedia.org/wiki/BB_9600
 - http://letraindalain.free.fr/galerieP.php?numserie=BB%209600

#### BB9700

 - https://fr.wikipedia.org/wiki/BB_9700
 - http://letraindalain.free.fr/galerieP.php?numserie=BB%209700

#### BB26000

 - https://fr.wikipedia.org/wiki/BB_26000
 - http://letraindalain.free.fr/galerieP.php?numserie=BB%2026000

#### BB67000

 - https://fr.wikipedia.org/wiki/BB_67000
 - http://letraindalain.free.fr/galerieP.php?numserie=BB%2067000

#### BB67200

 - https://fr.wikipedia.org/wiki/BB_67200

#### BB67300

 - https://fr.wikipedia.org/wiki/BB_67300

#### BB67400

 - https://twitter.com/ValentLuca/status/1386379443380363269/photo/1

#### BB69000

 - https://fr.wikipedia.org/wiki/BB_69000

#### BB70000

 - https://fr.wikipedia.org/wiki/BB_70000

#### CC6500

 - https://fr.wikipedia.org/wiki/CC_6500
 - http://letraindalain.free.fr/galerieP.php?numserie=CC%206500

#### CC1100

 - https://fr.wikipedia.org/wiki/CC_1100
 - http://letraindalain.free.fr/galerieP.php?numserie=CC%201100

#### CC7100

 - https://fr.wikipedia.org/wiki/CC_7100
 - http://letraindalain.free.fr/galerieP.php?numserie=CC%207100

#### CC65000

 - https://fr.wikipedia.org/wiki/CC_65000
 - http://letraindalain.free.fr/galerieD.php?numserie=CC%2065000

#### CC72000

 - https://twitter.com/ValentLuca/status/1386379443380363269/photo/1
 
#### TGV

 - http://letraindalain.free.fr/galerieTGV.php?numserie=TGV%20Sud-Est%20Bicourant%2001%20%E0%20102
 - https://fr.wikipedia.org/wiki/TGV_Sud-Est

#### X2800

 - https://fr.wikipedia.org/wiki/X_2800

#### X3600

 - http://letraindalain.free.fr/galerieA.php?numserie=X%203600%20ABJ4

#### X4300

 - https://fr.wikipedia.org/wiki/X_4300
 - http://letraindalain.free.fr/galerieA.php?numserie=X%204300

#### X4500

 - http://letraindalain.free.fr/galerieA.php?numserie=X%204500
 - https://fr.wikipedia.org/wiki/X_4500

#### X73500

 - https://fr.wikipedia.org/wiki/Liste_des_X_73500
 - http://letraindalain.free.fr/galerieA.php?numserie=X%2073500

### Outils - Tools

http://www.richardwheeler.net/interactive/pixeltool.html

### Merci

Merci au site *Le Train d'Alain* pour les photos !

Thanks for *Le Train d'Alain* for photos !
