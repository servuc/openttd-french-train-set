#!/bin/bash

##############################################################################

DATA_FILE="./data-trains.json"
GENERATED_FILE="./scripts/trains.pnml"
LANGUAGE_DIRECTORY="./lang"

##############################################################################

sanetize_alphanum_string () {
  echo "$1" | tr -cd '[:alnum:]'
}

##############################################################################

# Test if jq installed 

which jq > /dev/null 2>&1 
if [ "$?" != "0" ]
then
    echo "Please install jq (Json reader for bash)"
    exit 1
fi

# Test if data file exists
if [ ! -f "${DATA_FILE}" ]; then
    echo "${DATA_FILE} does not exist."
    exit 1
fi

# Test if data file can be parsed
jq . "${DATA_FILE}" > /dev/null 2>&1 
if [ "$?" != "0" ]
then
    echo "${DATA_FILE} is not a valid file"
    exit 1
fi

# Clear existing generated file
if [ -f "${GENERATED_FILE}" ]; then
    rm "${GENERATED_FILE}"
    echo "" > "${GENERATED_FILE}"
fi


# Prepare languages files
rm -rf "${LANGUAGE_DIRECTORY:?}/*.lng" 2>&1

for LANGUAGE_FILE in "${LANGUAGE_DIRECTORY}"/*.lng.template; do
    cp "${LANGUAGE_FILE}" "${LANGUAGE_FILE::-9}"
done



##############################################################################

generate_trains_new_line() {
    echo
}

sanetize_name_with_lang() {
    sanetize_alphanum_string "$(jq -r ".name.$2" <<< "$1")"
}

sanetize_name() {
    sanetize_name_with_lang "$1" "en"
}

sanetize_name_without_lang() {
    sanetize_alphanum_string "$(jq -r ".name" <<< "$1")"
}

generate_sprite_template_name() {
    printf "template_%s()" "$(sanetize_name_without_lang "$1")"
}

generate_sprite_template () {
    printf "template %s {\n\t%s\n}\n" "$(generate_sprite_template_name "$1")" "$(jq -r .sprites_templates <<< "$1")"
}

generate_name_for_livery () {
    MODEL="$(sanetize_name "$1")"
    if [ "${MODEL}" == "null" ]
    then
        echo "$(sanetize_name_without_lang "$2")_default"
    else
        echo "$(sanetize_name_without_lang "$2")_${MODEL}"
    fi
}

generate_name_for_spriteset_livery () {
    printf "spriteset_item_%s" "$(generate_name_for_livery "$1" "$2")"
}

generate_spriteset_for_livery () {
    JSON_FILE_FOR_SPRITE="$(jq .sprite <<< "$1")"
    printf "spriteset (%s , %s) { %s }" "$(generate_name_for_spriteset_livery "$1" "$2")" "${JSON_FILE_FOR_SPRITE}" "$(generate_sprite_template_name "$2")"
}

generate_translation_name() {
    TRAIN_NAME="$(sanetize_name_without_lang "$2")"

    if [ "$1" == "null" ]
    then
        LIVERY_NAME="null"
    else
        LIVERY_NAME="$(sanetize_name "$1")"
    fi

    if [ "${LIVERY_NAME}" == "null" ]
    then
        printf "STR_%s" "${TRAIN_NAME^^}"
    else
        printf "STR_%s_%s" "${TRAIN_NAME^^}" "${LIVERY_NAME^^}"
    fi
}

generate_item_for_livery () {
    ITEM_NAME="item_$(generate_name_for_livery "$1" "$2")"
    printf "item(FEAT_TRAINS, %s) {\n\tproperty {\n" "$ITEM_NAME"

    STRING_NAME="$(generate_translation_name "$1" "$2")"
    VARIANT_GROUP_ID="item_$(generate_name_for_livery "null" "$2")"

    if [ "$ITEM_NAME" == "$VARIANT_GROUP_ID" ]
    then
        PROPERTIES="$(jq add <<< "[ $3, $(jq .override_properties <<< "$2"), $(jq .override_properties <<< "$1"), {\"name\": \"string($STRING_NAME)\"} ]")"
    else
        PROPERTIES="$(jq add <<< "[ $3, $(jq .override_properties <<< "$2"), $(jq .override_properties <<< "$1"), {\"variant_group\": \"$VARIANT_GROUP_ID\"}, {\"name\": \"string($STRING_NAME)\"} ]")"
    fi

    jq -r 'keys[]' <<< "${PROPERTIES}" | while read KEY; do
        printf '\t\t%s: %s;\n' "${KEY}" "$(jq -r ".${KEY}" <<< "${PROPERTIES}")"
    done

    printf "\t}\n"
    
    printf "\tgraphics {\n"
    printf "\t\tadditional_text: return string(%s);\n" "$(generate_translation_name "$1" "$2")"   
    printf "\t\tdefault: %s;\n" "$(generate_name_for_spriteset_livery "$1" "$2")"    
    printf "\t}\n"
    printf "}" 
}

##############################################################################

complete_lang_files () {
    NAME_LENGTH="$(jq -r ".name | length" <<< "$2")"
    jq -c '.[]' <<< "$1" | while read LANG_CONFIGURATION; do
        LANG_NAME="$(jq -r '.name' <<< "${LANG_CONFIGURATION}")"
        LANG_REF="$(jq -r '.ref' <<< "${LANG_CONFIGURATION}")"

        if [ "${NAME_LENGTH}" == "0" ]
        then
            printf "%s : %s\n" "$(generate_translation_name "$2" "$3")" "$(jq -r ".name" <<< "$3")" >> "${LANGUAGE_DIRECTORY}/${LANG_NAME}.lng"
        else
            MODEL="$(jq -r ".name.${LANG_REF}" <<< "$2")"
            if [ "${MODEL}" != "null" ]
            then
                printf "%s : %s - %s\n" "$(generate_translation_name "$2" "$3")" "$(jq -r ".name" <<< "$3")" "${MODEL}" >> "${LANGUAGE_DIRECTORY}/${LANG_NAME}.lng"
            fi
        fi

    
    done
}

##############################################################################

I="1"
TOTAL_TRAINS="$(jq '.trains | length' "${DATA_FILE}")"
TOTAL_TRAINS_AND_LIVERIES="0"
LANGS_CONFIGURATION="$(jq -c '.available_languages' "${DATA_FILE}")"
generate_trains_new_line
jq -c '.trains[]' "${DATA_FILE}" | while read TRAIN_CONFIG; do
    {
        generate_sprite_template "${TRAIN_CONFIG}"
        generate_trains_new_line
    }  >> "${GENERATED_FILE}"

    TOTAL_LIVERIES="$(jq '.liveries | length' <<< "${TRAIN_CONFIG}")"
    TOTAL_TRAINS_AND_LIVERIES=$((TOTAL_TRAINS_AND_LIVERIES+TOTAL_LIVERIES))
    J="1"
    jq -c '.liveries[]' <<< "${TRAIN_CONFIG}" | while read TRAIN_LIVERY_CONFIG; do
        printf "[%02d/%02d - %02d/%02d - %03d] %s %s\n" "${I}" "${TOTAL_TRAINS}" "${J}" "${TOTAL_LIVERIES}" "${TOTAL_TRAINS_AND_LIVERIES}" "$(sanetize_name_without_lang "${TRAIN_CONFIG}")" "$(sanetize_name "${TRAIN_LIVERY_CONFIG}")"
        {
            generate_spriteset_for_livery "${TRAIN_LIVERY_CONFIG}" "${TRAIN_CONFIG}" 
            generate_trains_new_line
            generate_item_for_livery "${TRAIN_LIVERY_CONFIG}" "${TRAIN_CONFIG}" "$(jq -c .default_item_properties "${DATA_FILE}")"
            generate_trains_new_line
            generate_trains_new_line
        } >> "${GENERATED_FILE}"

        complete_lang_files "${LANGS_CONFIGURATION}" "${TRAIN_LIVERY_CONFIG}" "${TRAIN_CONFIG}" 

        J=$((J+1))
    done

    generate_trains_new_line >> "${GENERATED_FILE}"
    I=$((I+1))
done
generate_trains_new_line