//Licence from original file https://dev.openttdcoop.org/projects/dutchtracks/repository/entry/src/1500VDCgraphics.pnml
//  This file is part of The Dutch Track Set
//  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

//Catenary Poles

// 1500V - 1920
// 25kV - 1945

//No fences

template template_nofences() {
	[  0,     0,        1,     1,         0,        0, "gfx/rail/nofences.png"]
	[  0,     0,        1,     1,         0,        0, "gfx/rail/nofences.png"]
	[  0,     0,        1,     1,         0,        0, "gfx/rail/nofences.png"]
	[  0,     0,        1,     1,         0,        0, "gfx/rail/nofences.png"]
	[  0,     0,        1,     1,         0,        0, "gfx/rail/nofences.png"]
	[  0,     0,        1,     1,         0,        0, "gfx/rail/nofences.png"]
	[  0,     0,        1,     1,         0,        0, "gfx/rail/nofences.png"]
	[  0,     0,        1,     1,         0,        0, "gfx/rail/nofences.png"]
}

spriteset(spriteset_nofences) {
	template_nofences()
}

template template_rail_pylone() {
	[      1,       1,     8,     22,       -7, 	 -20]
	[     10,       1,     8,     24,        0, 	 -23]
	[     19,       1,     8,     22,        0, 	 -20]
	[     28,       1,     8,     24,       -7, 	 -23]
	[     37,       1,     2,     28,        0,  	 -21]
	[     40,       1,     2,     22,        0,  	 -22]
	[     43,       1,     8,     22, 	     0,  	 -23]
	[     52,       1,     8,     22,       -7, 	 -21]
}

template template_rail_catenaire() {
	[      1,       1,    32,     22,      -29, 	  -8]
	[     34,       1,    32,     22,       -1, 	  -8]
	[     67,       1,    32,      7,       -1, 	  -8]
	[    100,       1,     1,     22,        0, 	  -9]
	[    103,       1,    32,     30,      -29,  	  -8]
	[    135,       1,    32,     15,       -1,  	   0]
	[    168,       1,    32,     15, 	   -29,  	   1]
	[    201,       1,    32,     30,       -1, 	  -8]
	[    234,       1,    32,     19,      -29, 	  -5]
	[    267,       1,    32,     19,       -1, 	  -5]
	[    300,       1,    32,      7,       -1,  	  -8]
	[    333,       1,     1,     19,        0,  	  -6]
	[    336,       1,    32,     27, 	   -29,  	  -5]
	[    369,       1,    32,     12,       -1, 	   3]
	[    401,       1,    32,     12,      -29, 	   3]
	[    434,       1,    32,     27,       -1, 	  -5]
	[      1,       32,   32,     22,      -29, 	  -8]
	[     34,       32,   32,     22,       -1, 	  -8]
	[     67,       32,   32,      7,       -1, 	  -8]
	[    100,       32,    1,     22,        0, 	  -9]
	[    103,       32,   32,     20,      -29,  	  -8]
	[    135,       32,   32,     15,       -1,  	   0]
	[    168,       32,   32,     15, 	   -29,  	   0]
	[    201,       32,   32,     30,       -1, 	  -8]
	[    234,       32,   16,     11,      -29, 	   2]
	[    251,       32,   16,     14, 	    -1,  	  -8]
	[    268,       32,   16,     14,      -13, 	  -8]
	[    285,       32,   16,     11,       15, 	   3]		
}


spriteset (sprite_item_rail_1500v_pylone, "gfx/rail/pylone-1500V.png") { template_rail_pylone() }
spriteset (sprite_item_rail_1500v_catenaire, "gfx/rail/catenaire-1500V.png") { template_rail_catenaire() }

spriteset (sprite_item_rail_25kv_pylone, "gfx/rail/pylone-25kV.png") { template_rail_pylone() }
spriteset (sprite_item_rail_25kv_catenaire, "gfx/rail/catenaire-25kV.png") { template_rail_catenaire() }


//Licence from original file https://dev.openttdcoop.org/projects/dutchtracks/repository/entry/src/1500VDC.pnml
//  This file is part of The Dutch Track Set
//  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License along with 

//Defines Classic Electric line (1500 V DC, 160 km/h)

railtypetable {
	ELRL,
	RAIL,
    SBEd, //1500V  DC
    SBEA, //25000V AC
    FRBO: [SBEd, SBEA]
}

item(FEAT_RAILTYPES,item_1500V) {

	property {

		//Naming
		label:						"SBEd";
		name:						string(STR_NAME_RAIL_1500V);
		toolbar_caption:			string(STR_TOOL_RAIL_1500V);
		menu_text:					string(STR_MENU_RAIL_1500V);
		build_window_caption:		string(STR_BUIL_RAIL_1500V);
		autoreplace_text:			string(STR_REPL_RAIL_1500V);
		new_engine_text:			string(STR_NENG_RAIL_1500V);
		sort_order:					18;	

		//Properties of the rail
		compatible_railtype_list:	["RAIL","ELRL","SBEd","SBEA","FRBO"];
		powered_railtype_list:		["SBEd","FRBO"];
		acceleration_model:			ACC_MODEL_RAIL;

		//Costs
		construction_cost:			16;
		maintenance_cost:			14;

		//Graphics and other stuff
		station_graphics:			RAILTYPE_STATION_NORMAL;
		railtype_flags:				bitmask(RAILTYPE_FLAG_CATENARY);

		//Long list with fallback types:
		//alternative_railtype_list:	["ELRL"];	
			speed_limit: 			270 km/h;
	}

	graphics {
		//track_overlay:	spriteset_CLASSIC_ELRL_overlay;
		//underlay:			spriteset_CLASSIC_ELRL_underlay;
		//tunnels:			spriteset_CLASSIC_ELRL_tunnel;
		//level_crossings:  spriteset_CLASSIC_ELRL_levcross;
		//depots:           spriteset_CLASSIC_ELRL_depot;
		//bridge_surfaces:  spriteset_CLASSIC_ELRL_bridges;
		catenary_wire:		sprite_item_rail_1500v_catenaire;
		catenary_pylons:	sprite_item_rail_1500v_pylone;
		fences:				spriteset_nofences;
	}
}

item(FEAT_RAILTYPES,item_25KV) {

	property {

		//Naming
		label:						"SBEA";
		name:						string(STR_NAME_RAIL_25KV);
		toolbar_caption:			string(STR_TOOL_RAIL_25KV);
		menu_text:					string(STR_MENU_RAIL_25KV);
		build_window_caption:		string(STR_BUIL_RAIL_25KV);
		autoreplace_text:			string(STR_REPL_RAIL_25KV);
		new_engine_text:			string(STR_NENG_RAIL_25KV);
		sort_order:					19;	

		//Properties of the rail
		compatible_railtype_list:	["RAIL","ELRL","SBEd","SBEA","FRBO"];
		powered_railtype_list:		["SBEA","FRBO"];
		acceleration_model:			ACC_MODEL_RAIL;

		//Costs
		construction_cost:			18;
		maintenance_cost:			18;

		//Graphics and other stuff
		station_graphics:			RAILTYPE_STATION_NORMAL;
		railtype_flags:				bitmask(RAILTYPE_FLAG_CATENARY);

		//Long list with fallback types:
		//alternative_railtype_list:	["ELRL"];	
			speed_limit: 			650 km/h;
	}
	
	graphics {
		//track_overlay:	spriteset_CLASSIC_ELRL_overlay;
		//underlay:			spriteset_CLASSIC_ELRL_underlay;
		//tunnels:			spriteset_CLASSIC_ELRL_tunnel;
		//level_crossings:  spriteset_CLASSIC_ELRL_levcross;
		//depots:           spriteset_CLASSIC_ELRL_depot;
		//bridge_surfaces:  spriteset_CLASSIC_ELRL_bridges;
		catenary_wire:		sprite_item_rail_25kv_catenaire;
		catenary_pylons:	sprite_item_rail_25kv_pylone;
		fences:				spriteset_nofences;
	}
}