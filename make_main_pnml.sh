#!/bin/bash

echo "// GENERATED FILE BY $0" > ofts.pnml
echo "" >> ofts.pnml
while IFS= read -r myLine
do
    if [ ! -z "${myLine}" ] && [ "${myLine:0:1}" != "#" ]
    then
        cat "${myLine}" >> ofts.pnml
        echo "" >> ofts.pnml
    fi
done < "ofts.files"